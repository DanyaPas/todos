import { Component } from '@angular/core';

// interface User {
// name: string;
// age: number;
// email?: string;
// }

// class User {
//   name: string;
//   age: number;
//   email?: string;

//   constructor(name: string, age: number) {
//     this.name = name;
//     this.age = age;
//   }

//   sayHi(): string {
//     return 'Hi';
//   }
// }

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor() {
    // let a: number;
    // let b: string;
    // let c: boolean;
    // let arr: string[];
    // arr = ['sdfsdf', 'sdfsd'];

    // let obj: any;
    // obj = 'sdfsdf';
    // obj = 345;

    // // let user: User = {
    // //   name: 'Ivan',
    // //   age: 28,
    // // };

    // let user = new User('Bob', 34);
    // user.sayHi();

    // console.log(this.test(40).age);
  }

  // test(a: number): User {
  //   return new User('Ivan', a);
  // }
}
