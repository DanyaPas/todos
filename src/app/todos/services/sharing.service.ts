import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharingService {
  data: object

  constructor() { }
  
  setData(data): void {
    this.data = data
  }

  getData(): object{
    return this.data;
}
}
