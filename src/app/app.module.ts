import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { TodosModule } from './todos/todos.module';
import { TodosService } from './todos/services/todos.service';
import { HttpClientModule } from '@angular/common/http';
import { TodoInfoComponent } from './todos/todos/todo-info/todo-info.component';

@NgModule({
  declarations: [AppComponent, TestComponent, TodoInfoComponent],
  imports: [BrowserModule, AppRoutingModule, TodosModule, HttpClientModule],
  // providers: [TodosService],
  bootstrap: [AppComponent],
})
export class AppModule {}
