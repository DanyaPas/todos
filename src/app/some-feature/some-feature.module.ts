import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SomeFeatureRoutingModule } from './some-feature-routing.module';
import { SomeFeatureComponent } from './some-feature.component';
import { ChildFeatureComponent } from './child-feature/child-feature.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [SomeFeatureComponent, ChildFeatureComponent],
  imports: [CommonModule, SomeFeatureRoutingModule, HttpClientModule],
})
export class SomeFeatureModule {}
