import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent {
  title = 'Some title';
  subtitle = 'Some subtitle';
  counter = 0;

  arr = [
    {
      title: 'Item 1',
    },
    {
      title: 'Item 2',
    },
    {
      title: 'Item 3',
    },
  ];

  show = false;
  constructor() {
    // setInterval(() => {
    //   this.show = !this.show;
    // }, 1000);
    // setInterval(() => {
    //   this.arr.push({
    //     title: 'Item X',
    //   });
    // }, 1000);
    // setInterval(() => {
    //   this.counter += 1;
    // }, 1000);
  }
}
