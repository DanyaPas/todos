import { Component, OnInit, Output } from '@angular/core';
import { Todo } from '../models/todo';
import { TodosService } from '../services/todos.service';
import { SharingService } from '../services/sharing.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss'],
})
export class TodosComponent implements OnInit {
  todos: Todo[];
  newTodoTitle: string;
  userId: string;
  id: string;
  user;
  data;

  constructor(
    private todosService: TodosService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private SharingService: SharingService) { }

  ngOnInit(): void {
    this.todosService.getTodos().subscribe((todos: any) => {
      this.todos = todos;
    });
    this.router.events.subscribe(() => {
      console.log('it works');
      this.wrapUp();
      console.log('it works');
      this.SharingService.setData(this.data)
      console.log('it works');
    })
  }

  addTodo(event: Event): void {
    event.preventDefault();
    const title = this.newTodoTitle;

    this.todosService.addTodo(title).then((newTodo) => {
      this.todos = [...this.todos, newTodo];
      this.newTodoTitle = '';
    });
  }

  deleteTodo(id: string): void {
    this.todosService.deleteTodo(id).then((deletedTodoId) => {
      this.todos = this.todos.filter((todo) => todo.id !== deletedTodoId);
    });
  }

  getUser(): void {
    this.id = this.activatedRoute.snapshot.params.id;
    this.userId = this.todos.find((todo) => { todo.id === this.id }).userId
    this.user = this.todosService.getUser(this.userId)
  }

  wrapUp(): void {
    this.data = { name: this.user.name, email: this.user.email, id: this.id }
  }
}

