import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../users.service';

@Component({
  templateUrl: './child-feature.component.html',
  styleUrls: ['./child-feature.component.scss'],
})
export class ChildFeatureComponent implements OnInit {
  login: string;
  user: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private usersService: UsersService
  ) {}

  ngOnInit(): void {
    this.login = this.activatedRoute.snapshot.params.login;
    this.usersService.getUser(this.login).subscribe((userData) => {
      this.user = userData;
    });
  }
}
