import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestComponent } from './test/test.component';
import { TodosComponent } from './todos/todos/todos.component';
import { TodoInfoComponent } from "./todos/todos/todo-info/todo-info.component";

const routes: Routes = [
  {
    path: 'todos',
    component: TodosComponent,
    children: [
      {
        path: ':id',
        component: TodoInfoComponent,
      },
    ],
  },
  {
    path: 'users',
    loadChildren: () =>
      import('./some-feature/some-feature.module').then(
        (m) => m.SomeFeatureModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }

