import { Component, OnInit, Input } from '@angular/core';
import { TodosComponent } from '../todos.component'

@Component({
  selector: 'app-todo-info',
  templateUrl: './todo-info.component.html',
  styleUrls: ['./todo-info.component.scss']
})
export class TodoInfoComponent implements OnInit {
  @Input() data: object
  constructor() { }

  ngOnInit(): void {
  }

}
