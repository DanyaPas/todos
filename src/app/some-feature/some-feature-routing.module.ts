import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChildFeatureComponent } from './child-feature/child-feature.component';

import { SomeFeatureComponent } from './some-feature.component';

const routes: Routes = [
  { path: '', component: SomeFeatureComponent },
  { path: ':login', component: ChildFeatureComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SomeFeatureRoutingModule {}
