import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';

import { switchMap } from 'rxjs/operators';
import { UsersService } from './users.service';

@Component({
  selector: 'app-some-feature',
  templateUrl: './some-feature.component.html',
  styleUrls: ['./some-feature.component.scss'],
})
export class SomeFeatureComponent implements OnInit {
  users: any[] = [];

  constructor(private usersService: UsersService) {}

  ngOnInit(): void {
    this.usersService.getUsers().subscribe((users: any[]) => {
      this.users = users;
    });
  }
}

// class Observable {
//   subscribers = [];

//   subscribe(fn) {
//     this.subscribers.push(fn);
//   }

//   next(x: number) {
//     this.subscribers.forEach((fn) => fn(x));
//   }
// }

// const obs = new Observable();
// obs.subscribe((res) => {
//   console.log(res);
// });
// setInterval(() => {
//   obs.next(10);
// }, 1000);
