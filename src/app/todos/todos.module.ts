import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodosComponent } from './todos/todos.component';
import { FormsModule } from '@angular/forms';
import { TodosService } from './services/todos.service';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [TodosComponent],
  imports: [CommonModule, FormsModule, RouterModule],
  exports: [TodosComponent],
  // providers: [TodosService],
})
export class TodosModule {}
