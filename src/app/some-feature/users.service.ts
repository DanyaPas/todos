import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { filter, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get('https://api.github.com/users');
    // .pipe(
    //   filter(
    //     (users: any[]) => !users.find((user) => user.login === 'mojombo')
    //   )
    // );
  }

  getUser(login: string) {
    return this.http.get('https://api.github.com/users/' + login);
  }
}
