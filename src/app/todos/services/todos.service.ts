import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { promise } from 'selenium-webdriver';
import { Todo } from '../models/todo';
import {ActivatedRoute} from '@angular/router';

const DUMMY_TODOS: Todo[] = [
  {
    id: '1',
    title: 'Task 1',
    completed: true,
  },
  {
    id: '2',
    title: 'Task 2',
    completed: false,
  },
  {
    id: '3',
    title: 'Task 3',
    completed: false,
  },
];

@Injectable({
  providedIn: 'root',
})
export class TodosService {
  todos: Todo[] = DUMMY_TODOS;
  constructor(private http: HttpClient, private activatedRoute: ActivatedRoute) {}

  getTodos() {
    return this.http.get('https://jsonplaceholder.typicode.com/todos');
  }

  addTodo(title: string): Promise<Todo> {
    if (!title) {
      return;
    }

    const newTodo: Todo = {
      id: new Date().getTime().toString(),
      title,
      completed: false,
    };
    this.todos = [...this.todos, newTodo];
    return new Promise((resolve, reject) => {
      resolve(newTodo);
    });
  }

  deleteTodo(id: string): Promise<string> {
    // const deletedTodo = this.todos.find((todo) => todo.id === id);
    this.todos = this.todos.filter((todo) => todo.id !== id);
    return new Promise((resolve) => {
      resolve(id);
    });
  }

  getUser(userId: string) {
    return this.http.get('https://api.github.com/users/' + userId);
  }
}
