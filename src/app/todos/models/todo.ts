export interface Todo {
  id?: string;
  userId?: string;
  title: string;
  completed: boolean;
}

export interface User {
  id: number,
  name: string,
  username: string,
  email: string,
  address: {
    street: string,
    suite: string,
    city: string,
    zipcode: string,
    geo: {
      lat: string,
      lng: string
    }
  }
}

export interface Data {
  name: string;
  email: string;
  id: string
}